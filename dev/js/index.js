import { saludo, despedida } from "./modules/example"
import { activeMenu } from './modules/active-menu'
import { changeColor } from './modules/jq'
saludo()
despedida()
activeMenu()

setTimeout(function() {
    $('.custom-title').css('background-color', '#c6c6c6');
}, 5000);

$('.custom-title').click(function(e) {
    e.preventDefault();
    $(this).slideToggle();
});