![Logo boilerplate customize](https://cdn-images-1.medium.com/max/2000/1*l4xICbIIYlz1OTymWCoUTw.jpeg)

# boilerplate customize


Un workflow para proyectos web estáticos. Donde se podra: compilar archivos, crear
el bundle javascript, recargar automáticamente el navegador con los cambios, etc.

## Modo de uso

1. Clone este repositorio (aun no tiene instalacion por npm o yeoman)
2. Ejecute `npm install` (asegurese de tener npm actualizado y Nodejs en v6 como minimo)
3. Ejecute `gulp`
4. Disfrute

## Características de boilerplate customize:

* Usa gulp para automatizar tareas
* Esta basado en Sass y ES6.
* Compila Sass con autoprefixer y muestra los cambios en tiempo real
* Compila ES6 con soporte para módulos ES6 (importar y exportar modulos)
* Detecta nuevos archivos añadidos al proyecto sin tener que reiniciar gulp
* Crea los sourcemaps de los archivos compilados
* Tiene una estructura lista de estilos (con Sass) basada en SMACSS y ITCSS
* Tiene una estructura lista para HTML.
* Tiene una estructura lista para importar y exportar modulos ES6
* Esta añadida  Bootstrap 4 y JQuery v3.1.1


## Estructura

1. La carpeta dev contiene la estructura de archivos con la que trabajará
2. La carpeta public contiene los archivos compilados que deberan llevarse a producción
3. Para Sass importe sus partials desde `styles.scss`, el orden está indicado en el mismo archivo
5. Para Js, la carpeta `modules` contiene los módulos que serán importados desde `index.js`

Siéntase libre de usarlo y de reportar cualquier problema que encuentre o sugerencia que tenga.
boilerplate customize es gratis, open source y de la comunidad para la comunidad.

NOTA: Este proyecto fue personalizado en base a EDboilerplate gracias por el aporte a la comunidad EDboilerplate en Github: [https://github.com/escueladigital/EDboilerplate](https://github.com/escueladigital/EDboilerplate)
